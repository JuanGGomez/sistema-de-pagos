-- MySQL dump 10.13  Distrib 8.0.17, for Win64 (x86_64)
--
-- Host: localhost    Database: seba
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `listado`
--

DROP TABLE IF EXISTS `listado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `listado` (
  `idlistado` int(11) NOT NULL AUTO_INCREMENT,
  `usuarios_id` int(11) NOT NULL,
  `nombre` varchar(45) DEFAULT NULL,
  `idpago` int(255) DEFAULT NULL,
  `monto` int(255) DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  `fecha` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `eliminado` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`idlistado`),
  KEY `fk_listado_usuarios_idx` (`usuarios_id`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `listado`
--

LOCK TABLES `listado` WRITE;
/*!40000 ALTER TABLE `listado` DISABLE KEYS */;
INSERT INTO `listado` VALUES (30,123,'jamal',3,1500,'jamal 1500','2021-01-21 11:54:05',0),(43,123,'jamal',48,200,'jamal 200','2021-01-22 14:53:03',0),(44,123,'jamal',14,1200,'jamal 1200','2021-01-22 15:07:01',0),(45,123,'jamal',54,1200,'jamal 1200','2021-01-22 15:08:35',1),(67,123,'jamal',65,1000,'jamal 1000','2021-01-26 13:35:35',1),(68,157,'donovan',57,200,'donovan 200','2021-01-26 13:38:27',1),(69,157,'donovan',8965,200,'donovan 200','2021-01-26 13:39:04',1),(70,157,'donovan',675,100,'donovan 100','2021-01-26 13:42:30',0),(71,157,'donovan',60,300,'donovan 300','2021-01-27 11:56:19',1),(72,157,'donovan',199,100,'donovan 100','2021-01-27 11:59:27',1),(73,157,'donovan',575,100,'donovan 100','2021-01-27 12:00:03',1),(74,157,'donovan',5667,100,'donovan 100','2021-01-27 12:00:33',1),(75,157,'donovan',34,100,'dononvan 100','2021-01-27 12:01:50',0);
/*!40000 ALTER TABLE `listado` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-03-01 18:00:05
